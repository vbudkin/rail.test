## Installation

    docker-compose up -d --build site
    docker-compose run --rm composer update
    docker-compose run --rm npm install
    docker-compose run --rm npm run dev
    docker-compose run --rm artisan migrate
    docker-compose run --rm artisan users:create

## Links

    http://localhost:81/