<?php

use App\Http\Controllers\MediaController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api']], function () {

    Route::prefix('auth')->group(function () {
        Route::post('login', [AuthController::class, 'login']);
    });

    Route::group(['middleware' => ['auth:api']], function () {
        Route::prefix('media')->group(function () {
            Route::get('/', [MediaController::class, 'index'])->middleware('can:viewList,App\Media');
            Route::get('{media}/{fileName?}', [MediaController::class, 'show'])->middleware('can:view,media');
            Route::post('/', [MediaController::class, 'store'])->middleware('can:create,App\Media');
            Route::delete('{media}', [MediaController::class, 'destroy'])->middleware('can:delete,media');
        });
    });
});
