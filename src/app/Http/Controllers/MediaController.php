<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MediaRequest;
use App\Http\Resources\MediaCollection;
use App\Http\Resources\MediaResource;

class MediaController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $media = $user->media()->orderBy('id', 'desc')->paginate(10);
        return new MediaCollection($media);
    }

    public function show(Media $media, $fileName = null)
    {
        if ($fileName) {
            return $media->getBase64();
        }
        return MediaResource::make($media);
    }

    public function store(MediaRequest $request)
    {
        $user = Auth::user();
        $media = $user->addMediaFromRequest('file')->toMediaCollection('user-media', config('media-library.disk_name'));
        return MediaResource::make($media);
    }
    public function destroy(Media $media)
    {
        $deleted = false;
        try {
            $deleted = $media->delete();
        } catch (\Exception $e) {
        }
        return [
            'deleted' => $deleted
        ];
    }
}
