<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\AuthUserResource;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $token = auth('api')->attempt($request->validated());
        if (!$token) {
            return response()->json(['errors' => ['invalid_credentials' => ['Invalid login/password or user is inactive']]], 422);
        }
        $user = auth('api')->user();
        $user->token = $token;
        return AuthUserResource::make($user);
    }
}
