<?php

namespace App;

use Spatie\MediaLibrary\MediaCollections\Models\Media as SpatieMediaModel;

class Media extends SpatieMediaModel
{
    public function getBase64($thumb = false)
    {
        $path = $thumb ? $this->getPath('thumb') : $this->getPath();
        if (!file_exists($path)) {
            return null;
        }
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        return 'data:application/' . $type . ';base64,' . base64_encode($data);
    }
}
